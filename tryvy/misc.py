# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import locale
from datetime import datetime
import dateutil.tz
from babel.numbers import format_number as babel_fnumber, \
    parse_number as babel_pnumber, parse_decimal as babel_pdecimal
from babel.dates import format_date as babel_fdate
from kivy.utils import platform
from .tryton.maintenance import get_db_file_paths
import gzip
import shutil
import os

if platform == 'android':
    from jnius import autoclass

__all__ = ['NOT_ANDROID', 'get_default_language']

# Constants
NOT_ANDROID = ('linux', 'macosx')


def get_default_language():
    if platform == 'android':
        Locale = autoclass('java.util.Locale')
        current_lang = Locale.getDefault().getLanguage()
        return current_lang.split('.')[0] if current_lang else None
    else:
        return locale.getdefaultlocale()[0]


def format_number(value):
    # TODO: use float or decimal conversion
    if value is None or value == '':
        return ''
    if platform == 'android':
        return babel_fnumber(value, locale=get_default_language())
    return locale.str(value)


def unformat_number(value):
    # TODO: use float or decimal conversion
    if value is None or value == '':
        return None
    if platform == 'android':
        # parse_number is not fully implemented (http://babel.pocoo.org/en/latest/numbers.html)
        return float(babel_pdecimal(value, locale=get_default_language()))
    return locale.atof(value)


def format_date(value, date_format=None, time_zoned=False):
    if not value:
        return ''
    # TODO: standarize by language format
    _format = date_format
    if not _format:
        if isinstance(value, datetime):
            _format = '%d/%m/%y %H:%M'
        else:
            _format = '%d/%m/%y'
    if isinstance(value, str):
        value = datetime.strptime(value, '%y-%m-%d')
    return value.strftime(_format)


def compress_file(app_name, repl_database):
    cast = bytearray if bytes == str else bytes
    db_path = get_db_file_paths(app_name, repl_database)[0]
    with open(db_path, 'rb') as f_in, gzip.open(
            db_path + '.gz', 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)

    with open(db_path + '.gz', 'rb') as f:
        return cast(f.read())


def delete_file(app_name, repl_database):
    compressed_db_path = get_db_file_paths(app_name, repl_database)[0] + '.gz'
    if os.path.exists(compressed_db_path):
        os.remove(compressed_db_path)
