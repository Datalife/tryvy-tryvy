# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy import platform
from .api import with_pool, with_transaction, init as api_init

try:
    import tryton_replication
    from tryton_replication import ReplManager, SourceReplicateError
except ImportError:
    print('>>>> Cannot import tryton_replication')

from socket import gaierror
from tryton_rpc import Session
from tryton_rpc.exceptions import Fault
import sys
import traceback
from ..notify import (push_notif, PopNotifResponse, PopNotifRequest,
    has_nofications)
from ..misc import NOT_ANDROID, compress_file, delete_file
from ..osc import Response, Request, Reader
import threading
from threading import Thread, Condition, Event
from time import sleep
from trytond.pool import Pool
from functools import partial
from socket import error as socket_error, timeout as socket_timeout
from http.client import InvalidURL
import gettext
import os

if platform == 'android':
    from ..config import CONFIG
    from ..translate import setlang


_ = partial(gettext.dgettext, 'tryvy')


if platform == 'android':
    from jnius import autoclass
    Service = autoclass('org.kivy.android.PythonService')
    if Service.mService:
        Service.mService.setAutoRestartService(True)
    Intent = autoclass('android.content.Intent')

__all__ = ['repl_start', 'repl_stop', 'run_repl_service', 'activ_reader',
    'ActivityPausedRequest']


# Constants
_OSC_INTERVAL = 2
_OSC_MSG_APP_INFO = '/app_info'
_OSC_MSG_LOGIN = '/login'
_OSC_MSG_ACTIV_PAUSED = '/activity_paused'
_OSC_MSG_REPL_STATUS = '/replication_status'
_OSC_MSG_CONN_ERR = '/repl_conn_error'
# Activity constans
_ACTIVITY_PORT = 3002

# Service constans
_SERVICE_PORT = 3000


# Globals
_service = None
_repl_database = None
_session = None
_repl_interval = 60

# Activity globals
activ_reader = None
_app = None

# Service globals
_repl_thread = None
_must_init_db = True
_serv_reader = None
_uid = _pwd = _server = _port = _database = _app_name = None
_secondary_server = _secondary_port = None
_activ_paused = False
_session = None
_model_locale = {}
_textdomains = []

CONN_ERROR_TYPES = (gaierror, Fault, OSError, ConnectionRefusedError)

#
# Activity
#


class LoginInfoRequest(Request):
    message = _OSC_MSG_LOGIN

    def message_callback(self, *args):
        pass


class ActivityPausedRequest(Request):
    message = _OSC_MSG_ACTIV_PAUSED

    def message_callback(self, *args):
        pass


def _get_login_info():
    return [
        str(_app.uid),
        str(_app.pwd),
        str(_app.server),
        str(_app.port),
        str(_app.database),
        str(_repl_database),
        str(_app.secondary_server),
        str(_app.secondary_port)
    ]


class AppInfoResponse(Response):
    message = _OSC_MSG_APP_INFO

    def message_callback(self, *args):
        app_info = _get_login_info()
        if platform == 'android':
            for text_dom in _app._textdomains:
                app_info.append('text_domain')
                app_info.append(str(text_dom[0]))
                app_info.append(str(text_dom[1]))
        self.send(data_array=app_info)


class ReplicationStatusResponse(Response):
    message = _OSC_MSG_REPL_STATUS

    def message_callback(self, *args):
        """ args: [status, model names]"""
        if args[0] == 'finished':
            _app.replication_finished(args[1] and args[1].split(',') or [])
        elif args[0] == 'started':
            _app.login_text = _('Retrieving data ...')


class ReplicationConnectionErrorResponse(Response):
    message = _OSC_MSG_CONN_ERR

    def message_callback(self, *args):
        _app.replication_error(*args)


def _activ_osc_init():
    global activ_reader
    activ_reader = Reader(_ACTIVITY_PORT, _SERVICE_PORT,
        interval=_OSC_INTERVAL)
    activ_reader.request_append(PopNotifRequest)
    activ_reader.response_bind(AppInfoResponse)
    activ_reader.response_bind(ReplicationStatusResponse)
    activ_reader.response_bind(ReplicationConnectionErrorResponse)
    activ_reader.schedule_requests()


def _start_service_android():
    global _service, _must_init_db
    # in android db is downloaded ready to use
    _must_init_db = False
    activity = autoclass('org.kivy.android.PythonActivity').mActivity
    package_name = activity.getApplicationContext().getPackageName()
    _service = autoclass('%s.ServiceReplication' % package_name)
    argument = ''
    _service.start(activity, argument)


def _start_service_linux():
    global _service
    _service = Thread(target=run_repl_service)
    _service.setDaemon(True)
    _service.start()


def repl_start(app, repl_database=None):
    global _service
    global _app, _repl_database
    _app = app
    _repl_database = repl_database

    # set login info
    if activ_reader is None:
        _activ_osc_init()

    activ_reader.wait_for_response(LoginInfoRequest,
        data_array=_get_login_info(), need_response=False)

    if _service:
        repl_stop()
    print('Start replication service ...')
    if platform == 'android':
        _start_service_android()
    elif platform in NOT_ANDROID:
        _start_service_linux()
    else:
        raise NotImplementedError()


def _stop_service_android():
    global _service
    mActivity = autoclass('org.kivy.android.PythonActivity').mActivity
    _service.stop(mActivity)
    if _repl_thread:
        _repl_thread.stop()
    _service = None


def _stop_service_linux():
    global _service
    _repl_thread.stop()
    if _service.is_alive():
        _service.join(0)


def repl_stop():
    global _service
    if _service:
        print('Stop replication service ...')
        if platform == 'android':
            _stop_service_android()
        elif platform in NOT_ANDROID:
            _stop_service_linux()


#
# Service
#


class StoppableThread(Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self, *args, **kwargs):
        super(StoppableThread, self).__init__(*args, **kwargs)
        self._stop_event = Event()
        self._condition = Condition()

    def stop(self):
        self._stop_event.set()

    @property
    def stopped(self):
        return self._stop_event.is_set()

    def cond_acquire(self):
        self._condition.acquire()

    def cond_release(self):
        self._condition.release()


def _set_login_info(message):
    global _uid, _pwd, _server, _port, _database, _repl_database,\
        _must_init_db, _app_name, _secondary_server, _secondary_port
    _uid = message[0]
    _pwd = message[1]
    _server = message[2]
    _port = message[3]
    _database = message[4]
    _repl_database = message[5]
    _secondary_server = message[6]
    _secondary_port = message[7]
    _app_name = message[-1]
    _must_init_db = bool(platform != 'android')
    if platform == 'android':
        from android.storage import app_storage_path
        api_init(_repl_database, app_storage_path())


class AppInfoRequest(Request):
    message = _OSC_MSG_APP_INFO

    def message_callback(self, *args):
        _set_login_info(args)
        if platform == 'android':
            global _textdomains
            msgs = list(args)[6:]
            msgs.reverse()
            _textdomains = []
            msgs = iter(msgs)
            for _type, dirpath, appname in zip(msgs, msgs, msgs):
                if _type == 'text_domain':
                    _textdomains.append((dirpath, appname))
            setlang(
                lang=CONFIG['client.lang'],
                extra_textdomains=_textdomains)


def _get_app_info():
    _serv_reader.wait_for_response(AppInfoRequest)


class LoginResponse(Response):
    message = _OSC_MSG_LOGIN

    def message_callback(self, *args):
        global _repl_thread
        _repl_thread.cond_acquire()
        _set_login_info(args)
        _repl_thread.cond_release()


class ActivityPausedResponse(Response):
    message = _OSC_MSG_ACTIV_PAUSED

    def message_callback(self, *args):
        global _activ_paused
        message = args[0]
        _activ_paused = True if message != '0' else False


class ReplicationStatusRequest(Request):
    message = _OSC_MSG_REPL_STATUS

    def message_callback(self, *args):
        pass


class ReplicationConnectionErrorRequest(Request):
    message = _OSC_MSG_CONN_ERR

    def message_callback(self, *args):
        pass


def _serv_osc_init():
    global _serv_reader
    if _serv_reader:
        return
    _serv_reader = Reader(_SERVICE_PORT, _ACTIVITY_PORT,
        interval=_OSC_INTERVAL)
    _serv_reader.response_bind(PopNotifResponse)
    _serv_reader.response_bind(ActivityPausedResponse)
    _serv_reader.response_bind(LoginResponse)


def _send_notif_broadcast():
    if platform == 'android' and _activ_paused and has_nofications():
        service = Service.mService
        intent = Intent(Intent.ACTION_PROVIDER_CHANGED)
        service.sendBroadcast(intent)


def _notify_error(err):
    if isinstance(err, socket_timeout):
        push_notif(_('Timed out'))
    elif isinstance(err, socket_error):
        if err.errno == 111:
            push_notif(_('Connection refused'))
        elif err.errno == 113:
            push_notif(_('No route to host'))
        elif err.errno == 101:
            push_notif(_('Network is unreachable'))
        else:
            push_notif(str(err))
    elif isinstance(err, InvalidURL):
        push_notif(_('Invalid URL'))
    elif isinstance(err, SourceReplicateError):
        push_notif('%s: "%s"' % (_('Main Server is giving this error'),
            str(err)))
    else:
        push_notif(str(err))


def _rpc_login(second_server=False):
    global _session, _uid, _pwd, _server, _port, _database, \
        _secondary_server, _secondary_port
    if _session is not None:
        _session.logout()
    _session = Session()
    if second_server:
        _session.login(_uid, _pwd, _secondary_server,
            _secondary_port, _database)
    else:
        _session.login(_uid, _pwd, _server, _port, _database)


def is_upload_replica_db():
    global _session, _uid
    users = _session.execute('model', 'res.user', 'search_read',
            [('login', '=', _uid)], 0, None, None, ['upload_replica_db'])
    if not users:
        return False
    return users[0]['upload_replica_db']


def send_db():
    global _repl_database, _session
    if is_upload_replica_db():
        data = compress_file(_app_name, _repl_database)
        _session.execute('model', 'res.user', 'attach_db_to_user',
            _repl_database, data)
        delete_file(_app_name, _repl_database)


def _rpc_logout():
    global _session
    if _session is not None:
        _session.logout()
        _session = None


def _secure_init_db():
    global _must_init_db
    connection_error = False
    try:
        _rpc_login()
        _init_db(_session)
        _rpc_logout()
        _must_init_db = False
    except SourceReplicateError as e:
        traceback.print_exc(file=sys.stdout)
        _notify_error(e)
    except Exception as e:
        traceback.print_exc(file=sys.stdout)

        if type(e) == IOError:
            # local db does not exists so it is being downloaded
            # from remote server
            _must_init_db = False
            return

        connection_error = (type(e) in CONN_ERROR_TYPES)
        try:
            _rpc_login(second_server=True)
            _init_db(_session)
            _rpc_logout()
            _must_init_db = False
        except (Fault, gaierror):
            # cannot connect to server
            traceback.print_exc(file=sys.stdout)
            if connection_error:
                _send_connection_error()
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            _notify_error(e)


def _send_connection_error():
    _serv_reader.wait_for_response(ReplicationConnectionErrorRequest,
        data_array=[], need_response=False)


def _send_replication_status_request(data):
    _serv_reader.wait_for_response(ReplicationStatusRequest,
        data_array=list(data.values()),
        need_response=False)


@with_pool
@with_transaction(readonly=False)
def _get_locale_model_name(model_name):
    pool = Pool()
    IrModel = pool.get('ir.model')
    model, = IrModel.search([('model', '=', model_name)])
    _model_locale[model_name] = model.name


def get_locale_model_name(model_name):
    if model_name not in _model_locale:
        _get_locale_model_name(model_name)
    return _model_locale[model_name]


def send_log(exc_info):
    if platform == 'android':
        from android.storage import app_storage_path
        _log_path = os.path.join(app_storage_path(), 'data')
    elif platform in NOT_ANDROID:
        work_dir_path = os.getcwd()
        _log_path = os.path.join(work_dir_path, 'data')

    file_path = os.path.join(_log_path, 'replication.log')
    action = tryton_replication.current_action
    trace = "Error in action '{}' on model '{}'\n".format(action[0], action[1])
    trace += ''.join(traceback.format_exception(*exc_info))

    with open(file_path, 'w+') as file:
        data = file.read()
        if data != trace:
            _session.execute('model', 'replication.log',
                'register_log', str(exc_info[0]), trace)
            traceback.print_execution(*exc_info, file=open(file_path, "w"))


def _secure_replicate(in_service=True, **kwargs):

    def try_replicate(second_server=False):
        info = {}
        _log = False
        exc_info = None
        try:
            _rpc_login(second_server=second_server)
            # notify replication is started
            if in_service:
                _send_replication_status_request({
                    'status': 'started',
                    'models': ''
                })
            send_db()
            info = _replicate(_session, **kwargs)
            _rpc_logout()
        except socket_error as se:
            if second_server:
                _log = True
                _notify_error(se)
        except Exception:
            exc_info = sys.exc_info()
            _log = True

        if not _log:
            return info, True

        if exc_info:
            try:
                send_log(exc_info)
            except Exception:
                _notify_error(sys.exc_info()[0])
                return info, False

        return info, True

    for x in range(0, 2):
        info, res = try_replicate(x > 0)
        if info:
            break
        if not res:
            return bool(info)

    model_names = []
    for mod_info in info.get('pull', {}).items():
        locale_name = get_locale_model_name(mod_info[0])
        push_notif(_('%s: %s records received') % (locale_name, mod_info[1]))
        model_names.append(mod_info[0])
    for mod_info in info.get('push', {}).items():
        locale_name = get_locale_model_name(mod_info[0])
        if isinstance(mod_info[1], Exception):
            faultCode = getattr(mod_info[1], 'faultCode', None)
            push_notif(_('%s: %s') % (locale_name, faultCode or mod_info[1]))
        else:
            push_notif(_('%s: %s records sent') % (locale_name, mod_info[1]))
    if in_service:
        _send_replication_status_request({
            'status': 'finished',
            'models': ','.join(model_names)
        })

    return bool(info)


def configure_repl_interval(value):
    global _repl_interval
    _repl_interval = value


def _run_repl():
    global _must_init_db, _repl_interval

    def init_db():
        _secure_init_db()
        _send_notif_broadcast()

    def replicate():
        _secure_replicate()
        _send_notif_broadcast()

    if platform == 'android':
        _must_init_db = False
    cthread = threading.currentThread()
    while not cthread.stopped:
        cthread.cond_acquire()
        try:
            if _must_init_db:
                init_db()
            # re-check because it can be updated
            if not _must_init_db:
                replicate()
        except Exception:
            # ensure loop does not end suddenly
            traceback.print_exc(file=sys.stdout)
        cthread.cond_release()
        sleep(_repl_interval)


def run_repl_service():
    global _repl_thread
    _serv_osc_init()
    _get_app_info()
    _repl_thread = StoppableThread(target=_run_repl)
    try:
        _repl_thread.start()
    except Exception:
        traceback.print_exc(file=sys.stdout)
    finally:
        if _repl_thread.is_alive():
            _repl_thread.join(0)


@with_pool
@with_transaction(readonly=False)
def _replicate(rpc_session, **kwargs):
    return ReplManager.replicate(rpc_session, **kwargs)


@with_pool
@with_transaction(readonly=False)
def _init_db(rpc_session):
    ReplManager.init_db(rpc_session)


def _force_replicate(**kwargs):
    _set_login_info(_get_login_info())
    _secure_replicate(in_service=False, **kwargs)
