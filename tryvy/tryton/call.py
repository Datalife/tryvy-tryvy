# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from tryton_rpc import Session as RPCSession
from .maintenance import setup_tryton
from socket import error as socket_error

_persist_mode = None
_session = None

CONTEXT = {}


def login(persist_mode, app):
    global _persist_mode, _session

    def get_repl_database():
        if app.replication:
            return app.uid + '-' + app.server + '-' + app.database
        else:
            return app.repl_database

    if _session is not None:
        _session.logout()

    _persist_mode = persist_mode
    if persist_mode == 'REMOTE':
        _session = RPCSession()
        try:
            _session.login(app.uid, app.pwd, app.server, app.port,
                app.database
            )
        except socket_error:
            _session.login(app.uid, app.pwd, app.secondary_server,
                app.secondary_port, app.database)
        return True

    elif persist_mode == 'LOCAL':
        # import locally to not force trytond dependency
        from .api import init as api_init
        from .repl_service import repl_start
        repl_database = get_repl_database()
        api_init(repl_database, app.name)
        res, downloading_db = setup_tryton(app, repl_database)
        if app.replication:
            repl_start(app, repl_database=repl_database)
        if res and not downloading_db:
            # check if db already initialize (exists company)
            res = bool(execute('model', 'company.company', 'search_read',
                [], None, 1, None, ['id']))
        return res & (not downloading_db)


def logout():
    global _persist_mode, _session
    if _session is not None:
        _session.logout()
        _session = None
    _persist_mode = None


def execute(*args, **kwargs):
    global _persist_mode, _session
    callback = kwargs.get('callback', None)
    if callback:
        kwargs.pop('callback')
    _context = CONTEXT.copy()
    if kwargs.get('context'):
        _context.update(kwargs['context'])
    kwargs['context'] = _context
    res = None
    if _persist_mode == 'REMOTE' and _session is not None:
        res = _session.execute(*args, **kwargs)
    elif _persist_mode == 'LOCAL':
        # import locally to not force trytond dependency
        from .api import execute as api_execute
        res = api_execute(*args, **kwargs)
    if callback:
        return callback(res)
    return res
