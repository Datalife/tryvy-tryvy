#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivymd.uix.list import OneLineAvatarListItem
from kivy.properties import StringProperty
from kivy.lang import Builder

Builder.load_string('''
<TrytonOneLineLeftIconItem>

    IconLeftWidget:
        icon: root.icon
''')


class TrytonOneLineLeftIconItem(OneLineAvatarListItem):
    icon = StringProperty()
