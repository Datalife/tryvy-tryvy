#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.properties import DictProperty
from kivy.event import EventDispatcher
from tryvy.tryton.call import execute as call_execute
from .field import Field, format_record_values
from .pyson import PYSONDecoder
from decimal import Decimal


class Record(EventDispatcher):
    _record = DictProperty({}, rebind=True)

    def __init__(self, fields, context={}, light=False, model_name=None,
           **kwargs):
        super(Record, self).__init__(**kwargs)
        if fields:
            assert model_name
        self._fields = fields
        self.model_name = model_name
        self.state_attrs = {}
        self.modified_fields = {}
        self._loaded = []
        self.context = {}
        self.global_context = context.copy()
        self.values = {}
        if isinstance(fields, (dict, list)) and fields:
            if isinstance(fields, dict):
                self.values = format_record_values(fields)
                self.state = 'show'
            elif isinstance(fields, list):
                self.values = self.default_get(context=context)
                self._loaded = list(self.values.keys())
                for field in fields:
                    self.values.setdefault(field, None)
                self.values['id'] = -1
                self.state = 'create'
            self.context = self.get_eval_context(context, self.values)
            self.get_fields(self.values, self.context, light=light)
        else:
            self.state = 'deleted'

    def __getitem__(self, key):
        try:
            if self._record[key] is None:
                self._record[key] = Field(record=self, field_name=key)
                return self._record[key]
            return self._record[key]
        except KeyError:
            self._record[key] = Field(record=self, field_name=key)
            return self._record[key]

    def __setitem__(self, key, value):
        if isinstance(value, Field):
            self._record[key] = value
        elif key in self._record and self._record.get(key, None):
            self._record[key].value = value
        elif key not in self._record or self._record[key] is None:
            self._record[key] = Field(record=self, field_name=key, value=value)

    def __repr__(self):
        return '<Record {}@{} at {}>' .format(self.id.value, self.model_name,
            self._record)

    def __eq__(self, other):
        if not isinstance(other, Record):
            return NotImplemented
        elif self.id.value is None or other.id.value is None:
            return id(self) == id(other)
        return (self.model_name, self.id.value
            ) == (other.model_name, other.id.value)

    def __hash__(self):
        return hash((self.model_name, self.id.value))

    def get_eval_context(self, context, values):
        ctx = {'context': context.copy()}
        ctx.update(self.get_eval(values))
        return ctx

    def get_eval(self, values):
        value = {}
        for name, field in values.items():
            if name not in self._loaded and self.id.value >= 0:
                continue
            value[name] = field
        value['id'] = self.id.value
        return value

    def add_default_value(self, field_name, value, parent=False):
        self._record[field_name].value = value
        self.values[field_name] = value
        self.context.update({field_name: value})

    def is_simple_field(self, field_name):
        return '.' not in field_name

    def add_parent(self, field_name, parent_record, modified=True):
        self._record[field_name].value = parent_record['id'].value
        if not modified:
            self._record[field_name].set_not_modified()
        self.context['_parent_' + field_name] = dict(parent_record.values)
        changes = self.on_change(field_name, parent=True)
        if not changes:
            return
        self.set_on_change(changes)

    def get_fields(self, values, context, light=False):
        fields = []
        if not light:
            fields = call_execute('model', self.model_name, 'fields_get',
                self._fields_no_rec_name, context=context)

        for field_name, value in values.items():
            if field_name.endswith('.'):
                continue
            args = {
                'value': value,
                'record': self,
                'field_name': field_name
            }
            # rec name does not exist as field so we create it only with value
            if self.is_simple_field(field_name) and not light and \
                    field_name in fields:
                field_values = fields[field_name]
                field_type = field_values['type']
                model_name = None
                if field_type in ['many2one', 'one2many']:
                    model_name = field_values['relation']
                args.update({
                    'readonly': field_values['readonly'],
                    'states': field_values['states'],
                    'domain': field_values['domain'],
                    'label': field_values['string'],
                    'field_type': field_type,
                    'model_name': model_name,
                    'selection': field_values.get('selection', []),
                    'digits': field_values.get('digits', None),
                    'on_change_depends': field_values['on_change'],
                    'on_change_with_depends': field_values['on_change_with'],
                    'search_order': field_values.get('search_order', None),
                    'search_context': field_values.get('search_context', None),
                })
            self._record[field_name] = Field(**args)
            if self.is_simple_field(field_name) and not light:
                if value and field_name in self._loaded:
                    self.modified_fields.setdefault(field_name)
                    self._record[field_name].modified = True

    def default_get(self, rec_name=None, context={}):
        try:
            vals = call_execute('model', self.model_name, 'default_get',
                self._fields_no_rec_name, context=context)
        except Exception:
            return {}
        return format_record_values(vals)

    def update(self, _dict):
        self._record.update(_dict)

    def on_change(self, changed_field, parent=False):
        # changes from on_change_method
        if parent and self.id.value > 0:
            return []
        total_changes = self._record[changed_field].on_change()
        if not total_changes:
            total_changes = []
        # changes from every field whose on_change_with depends
        # matches the field_changed
        for field in self._fields_no_rec_name:
            depends = self._record[field].on_change_with_depends
            if changed_field in depends:
                changes = self._record[field].on_change_with()
                if changes:
                    total_changes.append(changes)

        for change in total_changes:
            # TODO process dict changes properly
            if change:
                to_remove = [key for key, value in change.items()
                    if isinstance(value, dict)]
                for key in to_remove:
                    change.pop(key, None)
        return total_changes

    def set_on_change(self, changes):
        for change in changes:
            for field_name, value in change.items():
                self[field_name] = value

    @property
    def _fields_no_rec_name(self):
        return [field for field in self._fields
            if self.is_simple_field(field)
            and not field.endswith('.')
        ]

    @property
    def modified(self):
        return bool(self.modified_fields)

    @property
    def id(self):
        if 'id' not in self._record or not self._record['id']:
            self._record['id'] = Field(value=-1, record=self, field_name='id')
        return self._record['id']

    def get(self):
        value = {}
        for name, field in self._record.items():
            if 'rec_name' in name:
                continue
            if not field.modified:
                continue
            if name not in self.modified_fields and self.id.value >= 0:
                continue
            if field.readonly:
                continue
            value[name] = field.convert_value
        return value

    def get_dict(self):
        value = {}
        for name, field in self._record.items():
            value[name] = field.value
        return value

    def save(self, force_reload=True):
        assert self.model_name
        if self.id.value < 0 or self.modified:
            if self.id.value < 0:
                value = self.get()
                res, = call_execute('model', self.model_name, 'create',
                    [value])
                self._record['id'].value = res
            elif self.modified:
                value = self.get()
                if value:
                    call_execute('model', self.model_name, 'write',
                        [self.id.value], value)
        self.clear_modified_fields()
        return self.id.value

    def clear_modified_fields(self):
        for modified_field in self.modified_fields:
            self._record[modified_field].modified = False
        self.modified_fields = {}

    def read(self):
        if not self.id.value or int(self.id.value) < 0:
            return
        values = call_execute('model', self.model_name, 'read',
            [self.id.value], list(self._record.keys()))
        if values:
            self.values = values[0]
            self.context.update(values[0])
            self.get_fields(values[0], self.context)
            self.clear_modified_fields()

    def copy(self, default={}):
        new_record_id, = call_execute('model', self.model_name, 'copy',
            [self.id.value], default)
        new_record, = call_execute('model', self.model_name, 'read',
            [new_record_id], list(self._record.keys()))
        return Record(new_record, model_name=self.model_name,
            context=self.context)

    def expr_eval(self, expr):
        if not isinstance(expr, str):
            return expr
        if not expr:
            return
        elif expr == '[]':
            return []
        elif expr == '{}':
            return {}
        ctx = self.get_eval_context(self.context, self.values)
        ctx['active_model'] = self.model_name
        ctx['active_id'] = self.id.value
        # TODO: add parent
        #if self.parent and self.parent_name:
        #    ctx['_parent_' + self.parent_name] = \
        #        common.EvalEnvironment(self.parent)
        val = PYSONDecoder(ctx).decode(expr)
        return val
