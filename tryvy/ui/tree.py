#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import sys
import time
import traceback
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import (StringProperty, ObjectProperty, NumericProperty,
    ListProperty)

from kivymd.uix.list import (ThreeLineAvatarListItem, ILeftBody,
    ThreeLineListItem, IRightBodyTouch, TwoLineAvatarIconListItem,
    ThreeLineAvatarIconListItem, OneLineAvatarListItem,
    TwoLineRightIconListItem, OneLineIconListItem)
from kivymd.uix.selectioncontrol import MDCheckbox
from kivymd.uix.button import MDFloatingActionButton
from kivymd.uix.label import MDLabel
from kivymd.uix.tab.tab import MDTabsCarousel
from kivymd.uix.refreshlayout import MDScrollViewRefreshLayout
from kivy.effects.scroll import ScrollEffect
from kivymd.utils import asynckivy
from kivy.metrics import dp
from .context import TrytonContext
from ..tryton.call import execute as call_execute
from ..common.common import ModelAccess
from .dialog import ErrorDialog, ConfirmDialog
from .model.record import Record
import gettext

_ = gettext.gettext
SENSIBILITY = 0.5
LONG_PRESS_TIME = 1.5

Builder.load_string('''
<TrytonTree>
    app: app
    container: container.__self__
    refresh_callback: self.swipe_down
    MDList:
        id: container

<TrytonTreeCheck>
    app: app
    container: container.__self__
    do_scroll_x: False
    MDList:
        id: container

<RightButton>
    size: dp(42), dp(42)
    disabled:  not self.opacity
    md_bg_color: gch(colors['Gray']['200'])
    elevation_normal: 1

<TreeButtonItem>
    RightButton:
        icon: root.button_icon
        opacity: root.button_opacity
        on_press: root.on_press(self)

<TreeAvatarSwitchButtonItem>
    SwitchButton:
        tree_item: root.__self__
        icon: root.default_icon
        on_press: root.on_press(self)
        opacity: 0 if root.is_button_invisible else 1

<TreeLeftRightIconItem>
    AvatarLabel:
        font_style: 'Icon'
        theme_text_color: 'Custom'
        text: root.left_text
        text_color: root.left_text_color
    RightButton:
        icon: root.button_icon
        opacity: root.button_opacity
        on_press: root.on_press(self)

<TreePaginateItem>
    bg_color: [0, 0, 0, .2]
    on_release: self.view.swipe_up()
    IconLeftWidget:
        icon: 'arrow-collapse-down'

''')


class TreeItemFactory(object):

    classes = {}

    def get(self, name):
        return self.classes.get(name, TreeAvatarItem)

    def register(self, values, override=False):
        if not override:
            for key, value in values.items():
                self.classes.setdefault(key, value)
        else:
            self.classes.update(values)


class TreeItemMixin(ModelAccess):
    record = ObjectProperty(Record({}), rebind=True)
    view = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(TreeItemMixin, self).__init__()
        if kwargs.get('record'):
            self.record = kwargs['record']
            self.record.context = self.context
        self.bind(on_release=lambda *x: self.show_record())

    def show_record(self, context=None):
        self.view.show_record(self.record, context=context)

    def _reload(self):
        if self.record and self.record['id'].value and \
                int(self.record['id'].value) > 0:
            self.record.read()
            self.rebind_record()

    def rebind_record(self):
        prop = self.property('record')
        prop.dispatch(self)

    def copy_record(self):
        self.view.activate_records(self.record['id'].value, True)
        self.view.copy_record()


class TreeOneLineAvatarItem(OneLineAvatarListItem, TreeItemMixin):
    pass


class TreeItem(ThreeLineListItem, TreeItemMixin):
    pass


class TreeAvatarItem(ThreeLineAvatarListItem, TreeItemMixin):
    pass


class TreeAvatarRightItem(ThreeLineAvatarIconListItem, TreeItemMixin):
    pass


class TreeAvatarCheckItem(ThreeLineAvatarIconListItem, TreeItemMixin):
    _txt_left_pad = NumericProperty(dp(20))

    def __init__(self, **kwargs):
        super(TreeAvatarCheckItem, self).__init__(**kwargs)
        self.add_widget(RightCheckbox(tree_item=self))


class TreeAvatarSwitchButtonItem(TwoLineAvatarIconListItem, TreeItemMixin):
    _txt_left_pad = NumericProperty(dp(20))

    def __init__(self, **kwargs):
        super(TreeAvatarSwitchButtonItem, self).__init__(**kwargs)
        self._clockev = None
        self._active_dialog = False

    def on_press(self, button=None):
        if button:
            button.switch()

    @property
    def default_icon(self):
        return self._button_icons[0]

    def on_touch_down(self, touch):
        super().on_touch_down(touch)
        if self.collide_point(*touch.pos):
            self._clockev = Clock.schedule_once(self.on_long_press,
                LONG_PRESS_TIME)

    def on_touch_up(self, touch):
        if not self._active_dialog:
            super().on_touch_up(touch)
        if self._clockev:
            self._clockev.cancel()

    def on_long_press(self, time):
        pass


class TreeButtonItem(TwoLineRightIconListItem, TreeItemMixin):
    button_icon = StringProperty('android')
    button_opacity = NumericProperty(1)

    def on_press(self, button=None):
        pass


class TreeLeftRightIconItem(ThreeLineAvatarIconListItem, TreeItemMixin):
    left_text = StringProperty()
    left_text_color = ListProperty([0, 0, 0, 0])
    button_icon = StringProperty('android')
    button_opacity = NumericProperty(1)

    def on_press(self, button=None):
        pass


class TreePaginateItem(OneLineIconListItem):
    view = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.ids._lbl_primary.halign = 'center'


class TrytonTreeMixin(ModelAccess, TrytonContext):
    _type = 'model'
    _name = ''
    _limit = 20
    _extra_pages = 0
    _order = []
    _fields = []
    item_cls = None
    app = ObjectProperty(None)
    container = ObjectProperty(None)
    records = []
    action_btn = ObjectProperty(None)
    _active_records = set()
    _model_fields = {}
    _many2one_record = ObjectProperty(None)
    _many2one_name = StringProperty(None)

    def __init__(self, **kwargs):
        super(TrytonTreeMixin, self).__init__(**kwargs)
        self.register_event_type('on_create')
        self.item_cls = TreeItemFactory().get(self.__class__.__name__)
        self.paginate = False
        self.set_create_flag(False)

    @property
    def model_fields(self):
        if not self._model_fields:
            values = call_execute('model', 'ir.model.field', 'search_read',
                [('model.model', '=', self.get_model_name())], 0, None, None,
                ['name', 'ttype'])
            if values:
                res = {}
                for v in values:
                    res.setdefault(v['name'], v.copy())
                    res[v['name']].pop('name')
                self._model_fields = res
        return self._model_fields

    def get_model_name(self):
        return self._name

    def activate_records(self, record_id, value):
        self.set_create_flag(False)
        if value:
            self._active_records.add(record_id)
        else:
            self._active_records.remove(record_id)

    def _get_record_domain(self):
        if self._many2one_name:
            return [(self._many2one_name, '=',
                self._many2one_record['id'].value)]
        else:
            return []

    def _get_app(self):
        return self.app

    def _reload(self, domain=[], clear=True, records=[]):
        ids = [record['id'].value if getattr(
            record['id'], 'value', None) else record['id']
            for record in records]
        _domain = self._get_record_domain() + domain
        if ids:
            _domain.append(('id', 'not in', ids))
        try:
            offset = (self._extra_pages * self._limit) if self._limit else None
            limit = self._limit - len(records) if self._limit else None
            items = call_execute(self._type, self.get_model_name(),
                'search_read', _domain, offset, limit, self._order,
                self._fields, context=self.context)
            if limit:
                self.paginate = (len(items) >= limit)
            self.records = [Record(item, light=True,
                    context=self.context, model_name=self.get_model_name())
                for item in records + (items or [])]
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            dialog = ErrorDialog()
            dialog.open(title=_('Connection error'),
                text=_('Cannot connect to server! ' +
                     'Please check the connection settings ' +
                     'and device connectivity.'),
                exception=e)
            return
        self._create_items(clear=clear)

    def _create_items(self, clear=True):
        timeout = time.time() + 1   # 1 second from now
        while self._creating_items:
            # do not create items while other thread creating another ones
            asynckivy.sleep(0)
            if time.time() > timeout:
                break
        self.set_create_flag(True)
        if clear:
            self._clear()
        elif (self.container.children
                and isinstance(self.container.children[0], TreePaginateItem)):
            self.container.remove_widget(self.container.children[0])

        async def create_items_in_list():
            for item in self.records:
                await asynckivy.sleep(0)
                self.container.add_widget(
                    self.item_cls(record=item, view=self))
            if self.paginate:
                self.container.add_widget(TreePaginateItem(
                    text=_('Load more ...'),
                    view=self))
            self.set_create_flag(False)

        self._active_records = set()

        asynckivy.start(create_items_in_list())

    def set_create_flag(self, value):
        self._creating_items = value

    def _clear(self, record_ids=[]):
        to_del = []
        for item in self.container.children:
            if (isinstance(item, TreePaginateItem)
                    or (issubclass(item.__class__, TreeItemMixin)
                        and (not record_ids
                            or item.record['id'] in record_ids))):
                to_del.append(item)

        for item in to_del:
            self.container.remove_widget(item)
        self.scroll_y = 1
        self._extra_pages = 0

    @property
    def tryton_screen(self):
        screen = self.parent.parent.parent
        if isinstance(screen, MDTabsCarousel):
            return self.parent
        return screen

    def show_record(self, record, context=None):
        self.tryton_screen.show_record(record, context=context)

    def new_record(self, **kwargs):
        self.tryton_screen.new_record(**kwargs)
        self.dispatch('on_create')

    def on_create(self, *args):
        pass

    def update_new_record(self, record):
        if self._many2one_record and self._many2one_name:
            record.add_parent(self._many2one_name, self._many2one_record)

    def delete_record(self):
        if not self._active_records:
            return
        dialog = ConfirmDialog()
        dialog.open(title=_('Delete records'),
            text=_('Selected records will be deleted. Are you sure?'),
            on_accept=self._delete_record)

    def _delete_record(self, result):
        if not result:
            return
        record_ids = [r['id'] for r in self._active_records]
        call_execute(self._type, self.get_model_name(), 'delete', record_ids)
        self._clear(record_ids=record_ids)
        self._active_records = set()

    def copy_record(self):
        if not self._active_records:
            return
        record_ids = list(self._active_records)
        try:
            call_execute(self._type, self.get_model_name(), 'copy',
                record_ids, {})
        except Exception as e:
            dialog = ErrorDialog()
            dialog.open(title=_('Error!'),
                text=_('Error copying record.'),
                exception=e)
            return
        self._reload()


class TrytonRefreshScrollEffect(ScrollEffect):
    """This class is simply based on _RefreshScrollEffect.
    Adds swipe up effect
    """

    min_scroll_to_reload = NumericProperty("-200dp")
    """Minimum overscroll value to reload."""

    def on_overscroll(self, scrollview, overscroll):
        if overscroll < self.min_scroll_to_reload:
            scroll_view = self.target_widget.parent
            scroll_view._did_overscroll = True
            return True
        else:
            return False


class TrytonTree(MDScrollViewRefreshLayout, TrytonTreeMixin):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.effect_cls = TrytonRefreshScrollEffect

    def swipe_down(self):
        self._extra_pages = 0
        Clock.schedule_once(self.swipe_refresh, 1)

    def swipe_refresh(self, *args, **kwargs):
        paginate = kwargs.get('paginate')

        async def swipe_refresh():
            if not self.activate_swipe():
                return
            await asynckivy.sleep(0)
            if (getattr(self.tryton_screen, 'filter_bar', None)
                    and not paginate):
                self.tryton_screen.filter_bar.on_filter_record(
                    self.tryton_screen.filter_bar.value)
            else:
                for last_item in self.container.children:
                    if issubclass(last_item.__class__, TreeItemMixin):
                        break
                self._reload(clear=not paginate)
                if paginate and last_item:
                    self.scroll_to(last_item)

            self.refresh_done()

        asynckivy.start(swipe_refresh())

    def swipe_up(self):
        # TODO: animation on button release
        self._extra_pages += 1
        Clock.schedule_once(lambda x: self.swipe_refresh(paginate=True), 1)

    def activate_swipe(self):
        return not isinstance(self.parent.parent.parent, MDTabsCarousel)


class AvatarLabel(ILeftBody, MDLabel):
    pass


class RightCheckbox(IRightBodyTouch, MDCheckbox):
    tree_item = ObjectProperty(None)

    def on_active(self, instance, value):
        super(RightCheckbox, self).on_active(instance, value)
        self.tree_item.view.activate_records(self.tree_item.record, value)


class RightButton(IRightBodyTouch, MDFloatingActionButton):
    pass


class SwitchButton(RightButton):
    tree_item = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._clockev = None

    def switch(self):
        icons = self.tree_item._button_icons
        self.icon = icons[1] if self.icon == icons[0] else icons[0]
