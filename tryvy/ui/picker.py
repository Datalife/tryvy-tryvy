#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
import locale
from kivymd.uix.pickers import MDTimePicker as KivyMDTimePicker

DEFAULT_LOCALE = 'en_US.utf8'


class MDTimePicker(KivyMDTimePicker):

    def _get_data(self):
        clocale = os.environ['LANG']
        if clocale != DEFAULT_LOCALE:
            locale.setlocale(locale.LC_TIME, DEFAULT_LOCALE)
            try:
                value = super()._get_data()
            finally:
                locale.setlocale(locale.LC_TIME, clocale)
        return value
